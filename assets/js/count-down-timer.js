// Set the date we are counting down to
var countDownDate = new Date("July 1, 2022 20:45:35").getTime();

//Update the count down every 1 second
var x = setInterval(function () {
  // Get today's date and time
  var now = new Date().getTime();

  //Find the distance between now and the count down date
  var distance = countDownDate - now;

  //Time calculations for days, hours, minutes, and seconds
  var days = Math.floor(distance / (1000 * 60 * 60 * 24));
  var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
  var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
  var seconds = Math.floor((distance % (1000 * 60)) / 1000);

  //   output the result in an element with id="demo"
  var z = document.getElementsByClassName("demos");
  for (var i = 0; i < z.length; i++) {
    z[i].innerHTML =
      '<span id="day">' +
      days +
      "</span>" +
      '<span id="hours">' +
      hours +
      "</span>" +
      '<span id="minutes">' +
      minutes +
      "</span>" +
      '<span id="second">' +
      seconds +
      "</span>";
  }

  // if the count down is over, write some text
  if (distance < 0) {
    clearInterval(x);
    var y = document.getElementsByClassName("demos");
    for (var i = 0; i < z.length; i++) {
      y[i].innerHTML = "";
    }

    var g = document.getElementsByClassName("after-offer-expire-text");
    for (var i = 0; i < z.length; i++) {
      g[i].classList.add("offer-expire-text");
    }

    var d = document.getElementsByClassName("offer-expire-text-inner");
    for (var i = 0; i < z.length; i++) {
      d[i].innerHTML = "پیشنهاد ویژه این محصول به پایان رسیده!";
    }
    var blur = document.getElementsByClassName("offer-end-blur");
    for (var i = 0; i < z.length; i++) {
      blur[i].style.filter = "blur(2px)";
    }
  }
}, 1000);
